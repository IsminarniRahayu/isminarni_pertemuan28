package stack_test

import (
	"pertemuan28/stack"
	"testing"
	"github.com/stretchr/testify/assert"
)

// requirements:
// - a stack is empty on construction 
// - a stack has size 0 on construction
// - after a pushes to an empty stack (n>0), the stack is non-empty and its size equals non
// - if one pushes  x then pops , the value popped is x, and the size is one less than old size
// - if one pushes x then peeks, the value returned is x, but the size stays the same
// - popping from an empty stack return an error: ErrNoSuchElement
// - peeking into an empty stack return an exception: ErrNoSuchElement

func TestNewSet(t *testing.T){
	t.Run("a stack is empty on construction", func(t *testing.T){
		s := stack.New()
		assert.True(t, s.IsEmpty())
	})

	t.Run("a stack has size 0 on construction", func(t*testing.T){
		s := stack.New()
		assert.Equal(t,0, s.Size())
	})

}

func TestInsert(t *testing.T){
	t.Run("after a pushes to an empty stack (n>0), the stack is non-empty and its size equals non", func(t* testing.T){
		s := stack.New()
		s.Push(1)
		s.Push(2)
		s.Push(3)
		assert.False(t, s.IsEmpty())
		assert.Equal(t,3, s.Size())
	})
	t.Run("if one pushes  x then pops , the value popped is x, and the size is one less than old size", func(t *testing.T){
		s := stack.New()
		s.Push(1)
		s.Push(2)
		s.Push(6)
		assert.Equal(t,3, s.Size())
		val, _:= s.Pop()
		assert.Equal(t, 6, val)
		assert.Equal(t,2, s.Size())
	})
	t.Run("if one pushes x then peeks, the value returned is x, but the size stays the same", func(t *testing.T){
		s := stack.New()
		s.Push(1)
		s.Push(2)
		s.Push(6)
		assert.Equal(t,3, s.Size())
		val, _:= s.Peek()
		assert.Equal(t, 6, val)
		assert.Equal(t,3, s.Size())
	})
}

func TestError(t *testing.T){
	t.Run("popping from an empty stack return an error: ErrNoSuchElement", func(t *testing.T){
		s := stack.New()
		_, err := s.Pop()
		if err == nil{
			t.Fail()
			t.Logf("Expect error is not nil, but got '%v'", err)
		}
		assert.Equal(t, stack.ErrNoSuchElemet, err)
	})

	t.Run("peeking into an empty stack return an exception: ErrNoSuchElement", func(t *testing.T){
		s := stack.New()
		_, err := s.Peek()
		if err == nil{
			t.Fail()
			t.Logf("Expect error is not nil, but got '%v'", err)
		}
		assert.Equal(t, stack.ErrNoSuchElemet, err)
	})
}